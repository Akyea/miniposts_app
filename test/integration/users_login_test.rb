require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
 
 def setup
  @user = users(:stanley)
 end
 
 def is_logged_in?
  !session[:user_id].nil?
 end
 
 test "login with invalid information" do
 
  get login_path
  assert_template 'sessions/new'
  post login_path, params: {session: {email: " ", password: " "}}
  assert_template 'sessions/new'
  assert_not flash.empty?
  get root_path
  assert flash.empty?
 end
 
 test "login with valid information and then logout" do
  
  # Visit the login path.
  get login_path
  
  # Post valid information to the sessions path.
  post login_path, params: {session: {email: @user.email, 
                                      password: "password"}}
                                      
   # Check if user is logged in
   assert is_logged_in?
   
   # Check the right redirect target            
   assert_redirected_to @user
   
   # Visit the target page
   follow_redirect!
   
   assert_template 'users/show'
   
  
   # Verify that the login link disappears.
   assert_select "a[href=?]", login_path, count: 0
   
   # Verify that a logout link appears
   assert_select "a[href=?]", logout_path
    
   # Verify that a profile link appears. 
   assert_select "a[href=?]", user_path(@user)
   
   # Use delete to issue a DELETE request to the logout path
   delete logout_path
   
   # Verify that the user is logged out and redirected to the root URL
  assert_not is_logged_in?
  assert_redirected_to root_url
  follow_redirect!
  
  # Simulate a user clicking logout in a second window.
  delete logout_path
  
  # Check that the login link reappears 
  assert_select "a[href=?]", login_path
  
  # Check that the logout and profile links disappear
  assert_select "a[href=?]", logout_path, count: 0
  assert_select "a[href=?]", user_path(@user), count: 0
 end
 
 test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_equal cookies['remember_token'], assigns(:user).remember_token
 end
 
 test "login without remembering" do
    # Log in to set the cookie.
    log_in_as(@user, remember_me: '1')
    # Log in again and verify that the cookie is deleted.
    log_in_as(@user, remember_me: '0')
    assert_empty cookies[:remember_token]
 end
 
end
