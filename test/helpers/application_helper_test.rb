require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "This is the Help Page for the Mini-Posts App"
    assert_equal full_title("Help"), "Help | Mini-Posts App"
  end
end
